import React from 'react';
import { Admin, Resource } from 'react-admin';
import { authClient, restClient } from 'ra-data-feathers';
import feathersClient from './feathersClient';

import { UserList, UserCreate, UserEdit } from './Views/Users';
import { AccountCircle } from '@material-ui/icons';

const authClientOptions = {
  storageKey: 'feathers-jwt',
  authenticate: { strategy: 'local' },
};

const options = { usePatch: true };

const App = () => (
  <Admin
    authProvider={authClient(feathersClient, authClientOptions)}
    dataProvider={restClient(feathersClient, options)}
  >
    <Resource 
      name="users"
      list={UserList}
      create={UserCreate}
      edit={UserEdit}
      options={{ label: 'Usuarios' }}
      icon={AccountCircle}
    />
  </Admin>
);


export default App;