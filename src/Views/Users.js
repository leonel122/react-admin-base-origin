import React from 'react';
import { List, Create, Edit, SimpleForm, DisabledInput, TextInput, Datagrid, TextField, EditButton, DeleteButton, SelectInput, ImageField, ImageInput  } from 'react-admin';

const UserTitle = ({ record }) => {
  console.log(record);
  return <span>User {record ? `"${record.first_name} ${record.last_name}"` : ''}</span>;
};


export const UserList = (props) => (
  <List {...props}>
    <Datagrid>
      <TextField source="id" />
    </Datagrid>
  </List>
);

export const UserCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
    
    </SimpleForm>
  </Create>
);

export const UserEdit = (props) => (
  <Edit title={<UserTitle />} {...props}>
    <SimpleForm>

    </SimpleForm>
  </Edit>
);