import feathers from "@feathersjs/client";

const host = 'https://backend.avantika.com.co:3033';
const authOptions = { jwtStrategy: 'jwt', storage: window.localStorage };

export default feathers()
  .configure(feathers.rest(host).fetch(window.fetch.bind(window)))
  .configure(feathers.authentication(authOptions));
