import React from 'react';
import { List, Create, Edit, SimpleForm, DisabledInput, TextInput, Datagrid, TextField, EditButton  } from 'react-admin';

const TagTitle = ({ record }) => {
  return <span>Tag {record ? `"${record.title}"` : ''}</span>;
};

export const TagList = (props) => (
  <List {...props}>
    <Datagrid>
      <TextField source="id" />
      <TextField source="title" />
      <EditButton />
    </Datagrid>
  </List>
);

export const TagCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="title" />
    </SimpleForm>
  </Create>
);

export const TagEdit = (props) => (
  <Edit title={<TagTitle />} {...props}>
    <SimpleForm>
      <DisabledInput label="Id" source="id" />
      <TextInput source="title"/>
    </SimpleForm>
  </Edit>
);